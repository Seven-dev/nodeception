const world = '🗺️';

export function hello(word: string = world): string {
  return `Hello ${world}! `;
  }

export interface Result {
    result: {};
    status: string;
}

// Replace with enum, class Node, etc...
export const readFileStatus = {
    success: 'success',
    error: 'error',
};
function _checkReadFile(path:string): Promise<string>{
    return Promise.resolve(path);
}
function _readFile(path:string): Promise<Result>{
    const results = {
        result:{ content: 'Weee'},
        status: readFileStatus.success,
    };
    return Promise.resolve(results);
}
export function readFileNode(path: string): Promise<Result>{
    return _checkReadFile(path).then((results)=>{
        return _readFile(path);
    });
}
