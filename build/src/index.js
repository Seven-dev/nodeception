"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const world = '🗺️';
function hello(word = world) {
    return `Hello ${world}! `;
}
exports.hello = hello;
// Replace with enum, class Node, etc...
exports.readFileStatus = {
    success: 'success',
    error: 'error',
};
function _checkReadFile(path) {
    return Promise.resolve(path);
}
function _readFile(path) {
    const results = {
        result: { content: 'Weee' },
        status: exports.readFileStatus.success,
    };
    return Promise.resolve(results);
}
function readFileNode(path) {
    return _checkReadFile(path).then((results) => {
        return _readFile(path);
    });
}
exports.readFileNode = readFileNode;
//# sourceMappingURL=index.js.map