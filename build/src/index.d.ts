export declare function hello(word?: string): string;
export interface Result {
    result: {};
    status: string;
}
export declare const readFileStatus: {
    success: string;
    error: string;
};
export declare function readFileNode(path: string): Promise<Result>;
